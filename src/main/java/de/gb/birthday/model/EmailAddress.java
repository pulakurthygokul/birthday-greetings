package de.gb.birthday.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailAddress
{
	private final String address;

	private EmailAddress(String address)
	{
		this.address = address;
	}

	public static EmailAddress of(String address)
	{
		validate(address);

		return new EmailAddress(address);
	}

	private static void validate(String address)
	{
		String regex = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*"
				+ "@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(address);

		if (!matcher.matches())
		{
			throw new IllegalArgumentException("'" + address + "' is not a valid email address.");
		}
	}

	public String address()
	{
		return address;
	}

	@Override
	public int hashCode()
	{
		return address.hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (!(obj instanceof EmailAddress))
		{
			return false;
		}

		EmailAddress other = (EmailAddress) obj;

		return address.equals(other.address);
	}
	
	public String toString()
	{
		return address;
	}
}
