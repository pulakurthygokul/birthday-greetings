package de.gb.birthday.model;

import java.time.LocalDate;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class EmployeeRecord
{
	private final String lastName;
	private final String firstName;
	private final LocalDate birthday;
	private final EmailAddress emailAddress;

	public EmployeeRecord(String lastName, String firstName, LocalDate birthday, EmailAddress emailAddress)
	{
		this.lastName = lastName;
		this.firstName = firstName;
		this.birthday = birthday;
		this.emailAddress = emailAddress;
	}

	public String lastName()
	{
		return lastName;
	}

	public String firstName()
	{
		return firstName;
	}

	public LocalDate birthday()
	{
		return birthday;
	}

	public EmailAddress emailAddress()
	{
		return emailAddress;
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()
			.append(birthday)
			.append(emailAddress)
			.append(firstName)
			.append(lastName)
			.hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
		{
			return true;
		}
		
		if(!(obj instanceof EmployeeRecord))
		{
			return false;
		}
		
		EmployeeRecord other = (EmployeeRecord) obj;
		
		return new EqualsBuilder()
			.append(this.birthday, other.birthday)
			.append(this.emailAddress, other.emailAddress)
			.append(this.firstName, other.firstName)
			.append(this.lastName, other.lastName)
			.isEquals();
	}

	@Override
	public String toString()
	{
		return String.format("EmployeeRecord[lastName=%s, firstName=%s, birthday=%s, emailAddress=%s]", lastName, firstName, birthday, emailAddress);
	}
}
