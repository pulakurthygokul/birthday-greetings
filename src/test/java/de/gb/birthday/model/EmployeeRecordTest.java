package de.gb.birthday.model;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;

import java.time.LocalDate;

import org.junit.Test;

public class EmployeeRecordTest
{
	@Test
	public void hasLastNameItWasCreatedWith()
	{
		String lastName = "Doe";
		
		EmployeeRecord employeeRecord = new EmployeeRecord(lastName, null, null, null);
		
		assertThat(employeeRecord.lastName(), equalTo(lastName));
	}
	
	@Test
	public void hasFirstNameItWasCreatedWith()
	{
		String firstName = "John";
		
		EmployeeRecord employeeRecord = new EmployeeRecord(null, firstName, null, null);
		
		assertThat(employeeRecord.firstName(), equalTo(firstName));
	}
	
	@Test
	public void hasDateItWasCreatedWith()
	{
		LocalDate birthday = LocalDate.of(1982, 10, 8);
		
		EmployeeRecord employeeRecord = new EmployeeRecord(null, null, birthday, null);
		
		assertThat(employeeRecord.birthday(), equalTo(birthday));
	}
	
	@Test
	public void hasEmailItWasCreatedWith()
	{
		EmailAddress emailAddress = EmailAddress.of("john.doe@foobar.com");
		
		EmployeeRecord employeeRecord = new EmployeeRecord(null, null, null, emailAddress);
		
		assertThat(employeeRecord.emailAddress(), equalTo(emailAddress));
	}
	
	@Test
	public void isEqualToSelf() throws Exception
	{
		EmployeeRecord john = johnDoe();
		
		assertThat(john, equalTo(john));
	}
	
	@Test
	public void isEqualToSameValue() throws Exception
	{
		EmployeeRecord john1 = johnDoe();
		EmployeeRecord john2 = johnDoe();
		
		assertThat(john1, equalTo(john2));
	}
	
	@Test
	public void isNotEqualWithDifferentFirstName() throws Exception
	{
		EmployeeRecord john = johnDoe();
		EmployeeRecord notJohn = johnWithOtherFirstName();
		
		assertThat(john, not(equalTo(notJohn)));
	}
	
	@Test
	public void isNotEqualToOtherClassInstance() throws Exception
	{
		EmployeeRecord john = johnDoe();
		
		assertThat(john, not(equalTo(new Object())));
	}
	
	@Test
	public void hasSameHashCodeAsItself() throws Exception
	{
		EmployeeRecord john1 = johnDoe();
		EmployeeRecord john2 = johnDoe();
		
		assertThat(john1.hashCode(), equalTo(john2.hashCode()));
	}
	
	@Test
	public void hasDifferentHashCodeWithDifferentFirstName() throws Exception
	{
		EmployeeRecord john = johnDoe();
		EmployeeRecord notJohn = johnWithOtherFirstName();
		
		assertThat(john.hashCode(), not(equalTo(notJohn.hashCode())));
	}
	
	
	private EmployeeRecord johnDoe()
	{
		EmployeeRecord johnDoe = new EmployeeRecord("Doe", "John", LocalDate.of(1982, 8, 10), EmailAddress.of("john.doe@foobar.com"));
		return johnDoe;
	}
	
	private EmployeeRecord johnWithOtherFirstName()
	{
		EmployeeRecord john = johnDoe();
		EmployeeRecord jane = new EmployeeRecord(john.lastName(), "Jane", john.birthday(), john.emailAddress());
		return jane;
	}
	
	@Test
	public void hasCorrectToString() throws Exception
	{
		String expectedToString = "EmployeeRecord[lastName=Doe, firstName=John, birthday=1982-08-10, emailAddress=john.doe@foobar.com]";
		
		String toString = johnDoe().toString();
		
		assertThat(toString, equalTo(expectedToString));
	}
}
