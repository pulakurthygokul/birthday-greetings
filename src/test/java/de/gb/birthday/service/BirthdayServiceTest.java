package de.gb.birthday.service;

import static java.util.Arrays.asList;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import de.gb.birthday.model.EmailAddress;
import de.gb.birthday.model.EmployeeRecord;
import de.gb.birthday.repository.EmployeeRepository;

@RunWith(MockitoJUnitRunner.class)
public class BirthdayServiceTest
{
	private static final String EXPECTED_SUBJECT = "Happy birthday!";

	@Mock
	private EmployeeRepository repository;

	@Mock
	private MessageService emailService;

	@InjectMocks
	private BirthdayService service;

	private LocalDate today;

	@Test
	public void loadsEmployeesFromRepository()
	{
		given(noEmployees());
		today = notImportant();
		
		service.sendGreetings(today);

		verify(repository).loadAll();
	}

	@Test
	public void sendsEmailIfBirthdayIsTodayFourtyFiveYearsAgo()
	{
		EmployeeRecord employee = given(employeeBornTodayMinusYears(45));
		today = LocalDate.now();
		
		service.sendGreetings(today);

		verifyGreetingSentFor(employee);
	}

	@Test
	public void sendsEmailIfBirthdayIsTodayThirtyTwoYearsAgo()
	{
		EmployeeRecord employee = given(employeeBornTodayMinusYears(32));
		today = LocalDate.now();
		
		service.sendGreetings(today);

		verifyGreetingSentFor(employee);
	}

	@Test
	public void sendsOneEmailForOneEmployeeBornToday() throws Exception
	{
		given(employeeBornTodayMinusYears(50), employeeNotBornToday());
		today = LocalDate.now();

		service.sendGreetings(today);

		verify(emailService, times(1)).sendGreeting(any(), any(), any());
	}
	
	@Test
	public void sendsTwoEmailsForTwoEmployeesBornToday() throws Exception
	{
		given(
			employeeBornTodayMinusYears(49), 
			employeeBornTodayMinusYears(48), 
			employeeNotBornToday());
		today = LocalDate.now();
		
		service.sendGreetings(today);
		
		verify(emailService, times(2)).sendGreeting(any(), any(), any());
	}

	@Test
	public void leapYearGreetingsAreSentOnFebruary28th() throws Exception
	{
		EmployeeRecord employee = given(employeeBornInLeapYear());
		today = LocalDate.of(2014, 2, 28);

		service.sendGreetings(today);

		verifyGreetingSentFor(employee);
	}

	@Test
	public void doesntSendEmailIfTodayIsNotTheBirthday()
	{
		given(employeeBornTodayMinusYears(20));
		today = LocalDate.MAX;
		
		service.sendGreetings(today);

		verifyZeroInteractions(emailService);
	}

	@Test
	public void doesntSendEmailIfBirthdayIsNotToday() throws Exception
	{
		given(employeeBornTodayMinusYearsMonthsDays(30, 1, 1));
		today = LocalDate.now();
		
		service.sendGreetings(today);
		
		verifyZeroInteractions(emailService);
	}

	private EmployeeRecord given(EmployeeRecord employee)
	{
		when(repository.loadAll()).thenReturn(asList(employee));
		return employee;
	}
	
	private List<EmployeeRecord> given(EmployeeRecord ... employees)
	{
		List<EmployeeRecord> employeeList = asList(employees);
		
		when(repository.loadAll()).thenReturn(employeeList);
		
		return employeeList;
	}
	
	private EmployeeRecord[] noEmployees()
	{
		return new EmployeeRecord[]{};
	}

	private EmployeeRecord employeeBornTodayMinusYears(int minusYears)
	{
		return employeeBornTodayMinusYearsMonthsDays(minusYears, 0, 0);
	}
	
	private EmployeeRecord employeeNotBornToday()
	{
		return employeeBornTodayMinusYearsMonthsDays(1, 1, 1);
	}

	private EmployeeRecord employeeBornTodayMinusYearsMonthsDays(int minusYears, int minusMonths, int minusDays)
	{
		return new EmployeeRecord("Doe", "John", 
				LocalDate.now()
					.minusYears(minusYears)
					.minusMonths(minusMonths)
					.minusDays(minusDays), 
				EmailAddress.of("john.doe@foobar.com"));
	}

	private EmployeeRecord employeeBornInLeapYear()
	{
		LocalDate leapYearBirthday = LocalDate.of(2004, 2, 29);
		return new EmployeeRecord("Jim", "Doe", leapYearBirthday, EmailAddress.of("jim.doe@foobar.com"));
	}
	
	private LocalDate notImportant()
	{
		return LocalDate.ofEpochDay(0);
	}

	private void verifyGreetingSentFor(EmployeeRecord ... employees)
	{
		for(EmployeeRecord employee : employees)
		{
			verify(emailService).sendGreeting(
				eq(EXPECTED_SUBJECT), 
				eq(expectedMessage(employee)), 
				eq(employee.emailAddress()));
		}
	}

	private String expectedMessage(EmployeeRecord employee)
	{
		return "Happy birthday, dear " + employee.firstName() + "!";
	}
}
