# README #

Trying out Alistair Cockburn's hexagonal architecture

### What is this repository for? ###

* Birthday greetings kata
* To learn hexagonal architecture
* [Kata description](http://matteo.vaccari.name/blog/archives/154)

### How do I get set up? ###

* Java 8
* Maven 3
* clone
* mvn clean install

### Contribution guidelines ###

* Write tests using
  * JUnit
  * Hamcrest Core
  * Mockito
* Follow hexagonal architecture principles

### Who do I talk to? ###

* Repo owner